#!/bin/bash

GITDIR="$1"
CONTENTDIR="$2"
BUILDDIR="$3"
NEWTAG="$4"


### Sanity checks ###

if [ ! -d "${GITDIR}/.git" ]; then
    echo "Error: ${GITDIR}/.git directory does not exist."
    exit 1
fi

if [ ! -d "${CONTENTDIR}" ]; then
    echo "Error: ${CONTENTDIR} directory does not exist."
    exit 1
fi

if [ ! -d "${BUILDDIR}" ]; then
    echo "Error: ${BUILDDIR} directory does not exist."
    exit 1
fi

if [ ! -f "${BUILDDIR}/src/genwiki" ]; then
    echo "Error: could not find genwiki in ${BUILDDIR}/src."
    exit 1
fi


### Information about tags ###

# Get a list of known tags from the git repository
tags=$(git --git-dir ${GITDIR}/.git tag)


# Check is the new tag is the latest version
is_latest=true
for tag in $tags; do
    IFS=.
    new=($NEWTAG)
    old=($tag)

    for ((i=${#old[@]}; i<${#new[@]}; i++)); do
        old[i]=0
    done
    for ((i=0; i<${#old[@]}; i++)); do
        if [[ -z ${new[i]} ]]; then
            new[i]=0
        fi
        if ((10#${new[i]} < 10#${old[i]})); then
            is_latest=false
	    break
        fi	
	if ((10#${new[i]} > 10#${old[i]})); then
	    break
        fi
    done

    unset IFS
done


### Download ###

# Create page with links to all the tarballs
downloads=$CONTENTDIR/download/previous.md
cat > $downloads << EOF
+++
title="Previous versions"
+++

EOF
for tag in $tags; do
    echo "* [libxc-${tag}.tar.gz](http://www.tddft.org/programs/libxc/down.php?file=${tag}/libxc-${tag}.tar.gz)" >> $downloads
done

# Add link do latest release
if [ "$is_latest" = true ] ; then
    cat > $CONTENTDIR/download/_index.md << EOF
+++
title = "Download"
weight = 20
pre = "<i class='fa fa-cloud-download'>&nbsp;</i> "
+++

# Latest release

The sources for the latest stable release can be downloaded here:

[libxc-${NEWTAG}.tar.gz](http://www.tddft.org/programs/libxc/down.php?file=${NEWTAG}/libxc-${NEWTAG}.tar.gz)


# Development version

You can also get Libxc directly from the [Gitlab repository](https://gitlab.com/libxc/libxc). This is the version you should use if you plan to develop Libxc.

EOF
fi


### ChangeLog ###

if [ "$is_latest" = true ] ; then
    cp ${GITDIR}/ChangeLog ${CONTENTDIR}/changes/ChangeLog.md
fi


### Functionals ###

# Create file, including hugo header
cat > $CONTENTDIR/functionals/previous/libxc-${NEWTAG}.md <<EOF
+++
title = "Libxc ${NEWTAG}"
+++

EOF
${BUILDDIR}/src/genwiki >> ${CONTENTDIR}/functionals/previous/libxc-${NEWTAG}.md

# If this is the latest release we need to do more things
if [ "$is_latest" = true ] ; then
    # Replace the old latest tag by the new one
    OLDTAG=$(ls ${CONTENTDIR}/functionals/libxc-*.md | sed "s|${CONTENTDIR}/functionals/libxc-||;s|.md||")
    mv $CONTENTDIR/functionals/previous/libxc-${NEWTAG}.md ${CONTENTDIR}/functionals/libxc-${NEWTAG}.md
    if [ x"${OLDTAG}" !=  x"${NEWTAG}" ]; then
        mv ${CONTENTDIR}/functionals/libxc-${OLDTAG}.md $CONTENTDIR/functionals/previous/libxc-${OLDTAG}.md
    fi

    # Update index so it points to the new tag
    cat > $CONTENTDIR/functionals/_index.md << EOF
+++
title = "Functionals"
weight = 50
pre = "<i class='fa fa-list-ul'>&nbsp;</i> " 
+++

{{% content "functionals/libxc-${NEWTAG}.md" %}}
EOF
fi

