+++
title = "Libxc"
+++

# Libxc

Libxc is a library of exchange-correlation and kinetic energy functionals for density-functional theory. The original aim was to provide a portable, well tested and reliable set of these functionals to be used by all the codes of the European Theoretical Spectroscopy Facility (ETSF), but the library has since grown to be used in several other types of codes as well; see below for a partial list.

Libxc is written in C, but it also comes with Fortran and Python bindings. It is released under the [MPL license (v. 2.0)](http://mozilla.org/MPL/2.0/). Contributions are welcome. Bug reports and patches should be submitted over [gitlab](https://gitlab.com/libxc/libxc).

To cite Libxc, the current reference is
* [Susi Lehtola, Conrad Steigemann, Micael J. T. Oliveira, and Miguel A. L. Marques, *Recent developments in Libxc - A comprehensive library of functionals for density functional theory*, Software X **7**, 1 (2018)](http://dx.doi.org/10.1016/j.softx.2017.11.002)

The previous reference to the library was
* [Miguel A. L. Marques, Micael J. T. Oliveira, and Tobias Burnus, *Libxc: a library of exchange and correlation functionals for density functional theory*, Comput. Phys. Commun. **183**, 2272 (2012).](http://dx.doi.org/10.1016/j.cpc.2012.05.007)

# Capabilities

In Libxc you can find various types of functionals: LDA, GGA, and meta-GGA (mGGA) functionals. LDAs, GGAs, and meta-GGAs depend on local information, in the sense that the value of the density functional part of the energy density at a given point depends only on the values of the density, the gradient of the density, and the kinetic energy density and/or the density laplacian, respectively, at the given point:

$$
E^\mathrm{LDA}\_\mathrm{xc} = E^\mathrm{LDA}\_\mathrm{xc}[n(\vec{r})],
$$

$$
E^\mathrm{GGA}\_\mathrm{xc} = E^\mathrm{GGA}\_{xc}[n(\vec{r}), \vec{\nabla}n(\vec{r})],
$$

$$
E^\mathrm{mGGA}\_\mathrm{xc} = E^\mathrm{mGGA}\_\mathrm{xc}[n(\vec{r}), \vec{\nabla}n(\vec{r}), \nabla^2 n(\vec{r}), \tau(\vec{r})].
$$

Libxc is designed to evaluate this energy density and its derivatives in a correct fashion. Because several functionals are complicated in form, Libxc is based on the use of computer algebra and automatic code generation to enable the generation of bug-free code. Libxc can calculate both the functional itself, as well as its first through fourth derivatives, satisfying even the stringest requirements for applications.

Global hybrid (GH) and range-separated hybrid (RSH) functionals are also supported by Libxc:
$$
E^\mathrm{GH}\_\mathrm{xc} = c_x E^\mathrm{EXX} + E^\mathrm{DFT}\_\mathrm{xc}[n(\vec{r}), \dots],
$$

$$
E^\mathrm{RSH}\_\mathrm{xc} = c_\mathrm{sr} E^\mathrm{EXX}_\mathrm{sr} + c_\mathrm{lr} E^\mathrm{EXX}_\mathrm{lr} + E^\mathrm{DFT}\_\mathrm{xc}[n(\vec{r}), \dots].
$$

For these functionals, Libxc only handles the local part (as above); the evaluation of the exact exchange components must be done in the calling program. Libxc, however, does contain all the information necessary to perform the calculations (fraction of exact exchange, range separation parameter(s)).

The same can be said about dispersion corrections: several functionals are available in Libxc that were parametrized with either semiclassical dispersion corrections à la Grimme, or various van der Waals functionals; neither of these can be evaluated with the local density information provided to Libxc, and must be handled by the calling program. The necessary parameters for VV10-type correlation kernels are, however, provided by Libxc as part of the functional definition.

# Support in codes

At the moment, we are aware of Libxc being used in the following codes (in alphabetical order):

* [Abinit](http://www.abinit.org/) - a software suite to calculate the optical, mechanical, vibrational, and other observable properties of materials
* [ACE-Molecule](https://gitlab.com/aceteam.kaist/ACE-Molecule/wikis/home) - a quantum chemistry package based on a real-space numerical grid
* [ADF](https://www.scm.com/adf-modeling-suite/) - a density functional theory program for molecules and condensed matter
* [APE](http://www.tddft.org/programs/APE) - a computer package designed to generate and test norm-conserving pseudopotentials within density functional theory
* [AtomPAW](http://www.wfu.edu/~natalie/papers/pwpaw/man.html) - a program for generating projector augmented wave functions
* [BAGEL](http://nubakery.org/) - a parallel electronic-structure program
* [BigDFT](http://bigdft.org/) - a fast, precise, and flexible density functional theory code for ab-initio atomistic simulation
* [CP2K](http://www.cp2k.org/ ) - a program to perform atomistic and molecular simulations of solid state, liquid, molecular, and biological systems
* [DFT-FE](https://sites.google.com/umich.edu/dftfe) - a massively parallel real-space code for first principles based materials modelling using Kohn-Sham density functional theory
* [DP](http://www.dp-code.org/) - a linear response time-dependent density functional theory code with a plane wave basis set
* [Chronus Quantum](http://www.chronusquantum.org/) - a computational chemistry software package focused on explicitly time-dependent and post-SCF methods
* [Elk](http://elk.sourceforge.net/) - an all-electron full-potential linearised augmented-plane wave code
* [entos](https://www.entos.info/about) -  a software package for Gaussian-basis ab initio molecular dynamics calculations on molecular and condensed-phase chemical reactions and other processes
* [ERKALE](https://github.com/susilehtola/erkale) - a DFT/HF molecular electronic structure code based on Gaussian-type orbitals
* [exciting](http://exciting-code.org/) - a full-potential all-electron density-functional-theory package implementing the families of linearized augmented planewave methods
* [FHI-AIMS](https://aimsclub.fhi-berlin.mpg.de) - an efficient, accurate, all-electron, full-potential electronic structure code package for computational molecular and materials science
* [GAMESS (US)](https://www.msg.chem.iastate.edu/GAMESS) - a general ab initio quantum chemistry package
* [GPAW](https://wiki.fysik.dtu.dk/gpaw) - a density-functional theory Python code based on the projector-augmented wave method
* [HelFEM](https://github.com/susilehtola/HelFEM) - Finite element methods for electronic structure calculations on small systems
* [Horton](http://theochem.github.io/horton/) - Python development platform for electronic structure methods
* [INQ](https://gitlab.com/npneq/inq/) - a modern GPU-accelerated computational framework for (time-dependent) density functional theory
* [JDFTx](http://sourceforge.net/p/jdftx/wiki/Home/) - plane-wave code designed for joint density functional theory
* [MADNESS](http://github.com/m-a-d-n-e-s-s/madness) - a multiwave adaptive numerical grid program for electroni
* [MOLGW](https://github.com/bruneval/molgw) - many-body perturbation theory for atoms, molecules, and clusters
* [Molpro](https://www.molpro.net) - a comprehensive system of ab initio programs for advanced molecular electronic structure calculations
* [MRCC](http://www.mrcc.hu/) - a suite of ab initio and density functional quantum chemistry programs for high-accuracy electronic structure calculations
* [NWChem](http://www.nwchem-sw.org/) - an open source, high-performance computational chemistry program
* [Octopus](http://octopus-code.org) - a program aimed at the ab initio virtual experimentation on a hopefully ever-increasing range of system types
* [OpenMolcas](https://gitlab.com/Molcas/OpenMolcas) - a quantum chemistry software package specializing in multiconfigurational approaches
* [ORCA](https://orcaforum.kofo.mpg.de/app.php/portal) - ab initio quantum chemistry program that contains modern electronic structure methods
* [PROFESS](http://www.princeton.edu/carter/research/software) - orbital-free density functional theory implementation to simulate condensed matter and molecules
* [Psi4](http://www.psicode.org/ ) - an open-source suite of ab initio quantum chemistry programs designed for efficient, high-accuracy simulations of molecular properties
* [PySCF](https://github.com/pyscf/pyscf) - Python-based Simulations of Chemistry Framework
* [QuantumATK](https://www.synopsys.com/silicon/quantumatk.html) - code including pseudopotential-based density functional theory methods with LCAO and plane-wave basis sets in one framework
* [Quantum Espresso](http://www.quantum-espresso.org/) - an integrated suite of open source computer codes for electronic-structure calculations and materials modeling at the nanoscale
* [Turbomole](http://www.turbomole.com/) - a program package for electronic structure calculations
* [Vasp](https://www.vasp.at/) - the Vienna Ab initio Simulation Package: atomic scale materials modelling from first principles
* [WIEN2k](http://www.wien2k.at) - program for electronic structure calculations of solids using density functional theory based on the full-potential (linearized) augmented plane-wave + local orbitals method
* [Yambo](http://www.yambo-code.org/) - a program that implements many-body perturbation theory methods such as GW and BSE and time-dependent density functional theory
