+++
title = "Download"
weight = 20
pre = "<i class='fa fa-cloud-download'>&nbsp;</i> "
+++

# Latest release

The sources for the latest stable release can be downloaded here:

[libxc-6.0.0.tar.gz](http://www.tddft.org/programs/libxc/down.php?file=6.0.0/libxc-6.0.0.tar.gz)


# Development version

You can also get Libxc directly from the [Gitlab repository](https://gitlab.com/libxc/libxc). This is the version you should use if you plan to develop Libxc.

